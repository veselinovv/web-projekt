<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\HomepageModel;

final class HomepagePresenter extends BasePresenter
{
    /** @var HomepageModel @inject */
	public $hm;

    public function __construct(HomepageModel $hm) {
		$this->hm = $hm;
	}

    public function renderDefault(): void {
		$this->template->products = $this->hm->productsPreview();
	}
}
