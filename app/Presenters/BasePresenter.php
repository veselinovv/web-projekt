<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;

/**
 * Třída BasePresenter
 * Zde se kontroluje zda je uživatel přihlašen a jakou má roli. Podle toho se posílají data na template.
 * 
 * @author Viktor Veselinov
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	///** @var BaseModel @inject */
	//public $baseModel;
	
	public function beforeRender(){
		parent::beforeRender();

		$odkazyArray = array(
			array("Homepage","Home"),
			array("Products","Products"),
			$this->getUser()->isLoggedIn() ? $this->getUser()->getIdentity()->role == "admin" ? array("Admin","Administration") : null : null
		);

		$role = isset($this->getUser()->getIdentity()->role) ? 
            $this->getUser()->getIdentity()->role:
            "guest";
            
        switch($role){
            case "admin":  
				$actions["Sign-out"] = "signout";
				break;          
            case "customer": 
                $actions["Sign-out"] = "signout";
				break;
            case "guest":
            default:
                $actions["Sign-in"] = "signin";
                $actions["Sign-up"] = "signup";
        }

		if(isset($this->getUser()->getIdentity()->name) && isset($this->getUser()->getIdentity()->surname))
		{
			$user["name"] = $this->getUser()->getIdentity()->name." ".$this->getUser()->getIdentity()->surname;
			$user["role"] = $this->getUser()->getIdentity()->role;
			$this->template->userInfo = $user;
		}

		$this->template->menuItems = $odkazyArray;
		$this->template->actions = $actions;
	}
}