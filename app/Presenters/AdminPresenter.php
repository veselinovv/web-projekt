<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms;
use Nette\Application\UI\Form;
use App\Model\AdminModel;
use App\Model\MyAuthenticator;

/**
 * Třída AdminPresenter
 * Zde se zpracovávají všechny formuláře na stránce administrace.
 * 
 * @author Viktor Veselinov
 */
final class AdminPresenter extends BasePresenter
{
    /** @var AdminModel @inject */
	public $am;

    /** @var MyAuthenticator @inject */
	public $ma;

    /** @persistent */
	public $customer = 0;

    /** @persistent */
	public $product = 0;

    /** @persistent */
	public $resC = 0;

    public function __construct(AdminModel $am) {
		$this->am = $am;
	}

    /**
    * Funkce startup
    * Kontroluje zda jsme přihlášeni při otevření stránky administrace a zda máme přístup k téhle stránce.
    *
    * @author Viktor Veselinov
    */
    public function startup(){
        parent::startup();
        if(!$this->getUser()->isLoggedIn() && $this->getAction() !== 'Sign:in' || $this->getUser()->getIdentity()->role != "admin"){
            $this->redirect('Sign:in');
            $this->terminate();
        }
    }

    /**
    * Funkce actionDefault
    * Kontroluje zda je nějaký product nebo zákazník vybraný k úpravě. Pokud ano, tak pošle jejich data na template. Pokud ne tak pošle hodnoty prázdné.
    *
    * @author Viktor Veselinov
    */
    public function actionDefault()
    {
        if($this->customer == 0){
            $resC = array("id" => "", "email" => "", "name" => "", "surname" => "", "role" => "");
        } else {
            $resC = (array) $this->am->getCustomer($this->customer);
        }
        $this->template->editCustomer = $resC;

        if($this->product == 0){
            $resP = array("id" => "", "name" => "", "price" => "", "description" => "", "link" => "");
        } else {
            $resP = (array) $this->am->getProduct($this->product);
        }
        $this->template->editProduct = $resP;
    }

    /**
    * Funkce createComponentSelectCustomerForm
    * Vytváří formulář kde můžem vybrat jakého zákazníka chceme upravovat.
    *
    * @author Viktor Veselinov
    * @return Form
    */
    public function createComponentSelectCustomerForm() : Form {
        $form = new Form();
        $form->addSelect('select', 'Vyber zákazníka k úpravě', $this->am->customersArray())
            ->setDefaultValue($this->customer)
            ->setHtmlAttribute('onchange', 'submit()');
        $form->onSuccess[] = [$this, 'selectCustomerFormSuccess'];
        return $form;
    }
    /**
    * Funkce selectCustomerFormSuccess
    * Po úspěšném výběru zákazníka pošle data o něm na template.
    *
    * @author Viktor Veselinov
    */
    public function selectCustomerFormSuccess(Form $form){
        $this->customer = $form->getValues()->select;
        $this->redirect('Admin:default');
    }

    /**
    * Funkce createComponentCustomerForm
    * Vytváří samotný formulář kde můžeme data o zákazníkovi přepsat nebo zákazníka úplně smazat.
    *
    * @author Viktor Veselinov
    * @return Form
    */
    public function createComponentCustomerForm() : Form {
        $form = new Form();
        $form->addHidden('id');
        $form->addText('email', 'E-mail');
        $form->addText('name', 'Name');
        $form->addText('surname', 'Surname');
        $form->addText('price', 'Price');
        try{
        $form->addSelect('role', 'Select a role', ['customer' => 'customer','admin' => 'admin'])
            ->setDefaultValue(isset($this->am->getCustomer($this->customer)->role) ?
                $this->am->getCustomer($this->customer)->role : $this->resC["role"]
            );
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addText('role', 'Role');
        }
        $form->addSubmit('edit', 'Edit');
        $form->addSubmit('delete', 'Delete')
            ->setHtmlAttribute('onclick', 'return confirm("Do you really want to delete this customer?");');
        $form->onSuccess[] = [$this, 'customerFormSuccess'];
        return $form;
    }
    /**
    * Funkce customerFormSuccess
    * Po zmáčknutí jednoho ze dvou tlačítek ve formuláři se spustí příslušná funkce(update nebo delete).
    *
    * @author Viktor Veselinov
    */
    public function customerFormSuccess(Form $form){
        $v = $form->getValues();
        switch($form->isSubmitted()->getValue()){
            case 'Edit':
                $result = $this->am->updateCustomer($v->id, $v);
                break;
            
            case 'Delete':
                $result = $this->am->deleteCustomer($v->id);
                break;
        }
        $this->customer = 0;
        if($result[0]){
            $this->flashMessage($result[1], "success");
        } else {
            $this->flashMessage($result[1], "danger");
        }
        $this->redirect('Admin:default');
    }

    /**
    * Funkce createComponentSelectProductForm
    * Vytváří formulář kde můžem vybrat jaký produkt chceme upravovat.
    *
    * @author Viktor Veselinov
    * @return Form
    */
    public function createComponentSelectProductForm() : Form {
        $form = new Form();
        $form->addSelect('select', 'Select a product', $this->am->productsArray())
            ->setDefaultValue($this->product)
            ->setHtmlAttribute('onchange', 'submit()');
        $form->onSuccess[] = [$this, 'selectProductFormSuccess'];
        return $form;
    }
    /**
    * Funkce selectProductFormSuccess
    * Po úspěšném výběru produktu pošle data o něm na template.
    *
    * @author Viktor Veselinov
    */
    public function selectProductFormSuccess(Form $form){
        $this->product = $form->getValues()->select;
        $this->redirect('Admin:default');
    }

    /**
    * Funkce createComponentProductForm
    * Vytváří samotný formulář kde můžeme data o produktu přidat, přepsat nebo produkt úplně smazat.
    *
    * @author Viktor Veselinov
    * @return Form
    */
    public function createComponentProductForm() : Form {
        $form = new Form();
        $form->addHidden('id');
        $form->addText('name', 'Product name');
        $form->addText('price', 'Price');
        $form->addText('description', 'Description');
        $form->addUpload('link', 'Product photo')
            ->addRule($form::IMAGE , "The uploaded file is not a photo!");
        $form->addSubmit('edit', 'Edit');
        $form->addSubmit('delete', 'Delete')
            ->setHtmlAttribute('onclick', 'return confirm("Do you really want to delete this product?");');
        $form->addSubmit('add', 'Add');
        $form->onSuccess[] = [$this, 'productFormSuccess'];
        return $form;
    }
    /**
    * Funkce productFormSuccess
    * Po zmáčknutí jednoho ze dvou tlačítek ve formuláři se spustí příslušná funkce(add, update nebo delete).
    *
    * @author Viktor Veselinov
    */
    public function productFormSuccess(Form $form){
        $v = $form->getValues();
        switch($form->isSubmitted()->getValue()){
            case 'Edit':
                $result = $this->am->updateProduct($v->id, $v, $v->link); // neodesila se link pri editu?? nevim proc xd
                break;
            
            case 'Delete':
                $result = $this->am->deleteProduct($v->id);
                break;

            case 'Add': 
                $result = $this->am->newProduct(["name" => $v->name, "price" => $v->price, "description" => $v->description], $v->link);
                break;
        }
        $this->product = 0;
        if($result[0]){
            $this->flashMessage($result[1], "success");
        } else {
            $this->flashMessage($result[1], "danger");
        }
        $this->redirect('Admin:default');
    }
}
