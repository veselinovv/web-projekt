<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\ProductsModel;

final class ProductsPresenter extends BasePresenter
{
    /** @var ProductsModel @inject */
	public $pm;

    public function __construct(ProductsModel $pm) {
		$this->pm = $pm;
	}

    public function renderDefault(): void {
		$this->template->products = $this->pm->productsPreview();
	}
}
