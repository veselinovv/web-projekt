<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms;
use App\Model;
use Nette\Application\UI\Form;

final class SignPresenter extends BasePresenter
{
	private $authenticator;

	public function __construct(Model\MyAuthenticator $authenticator)
	{
		$this->authenticator = $authenticator;
	}

	/* public function startup() {        
        parent::startup();
		if(!$this->getUser()->isLoggedIn() && $this->getAction() !== 'in' ){
			$this->redirect("Sign:in");
			$this->terminate();
		}
	} */

    public function actionOut(): void
    {
        $this->getUser()->logout(true);
        //$this->flashMessage('Odhlášení bylo úspěšné.');
        $this->redirect('Homepage:');
    }

	protected function createComponentSignInForm(): Form
	{
		$form = new Form;
		$form->addText('email', 'E-mail:')
			->setRequired('Prosím vyplňte své uživatelské jméno.');

		$form->addPassword('password', 'Password:')
			->setRequired('Prosím vyplňte své heslo.');

		$form->addSubmit('send', 'Sign in');

		$form->onSuccess[] = [$this, 'logInWithAuthenticator'];
		return $form;
	}

    public function signInFormSucceeded(Form $form, \stdClass $data): void
	{
		try {
			$this->getUser()->login($data->email, $data->password);
			$this->redirect('Homepage:');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Nesprávné přihlašovací jméno nebo heslo.');
		}
	}

    protected function createComponentSignUpForm(): Form
	{
		$form = new Form;
		$form->addText('email', 'E-mail:')
			->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addText('name', 'Name:')
			->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addText('surname', 'Surname:')
			->setRequired('Prosím vyplňte své uživatelské jméno.');

		$form->addPassword('password', 'Password:')
			->setRequired('Prosím vyplňte své heslo.');

		$form->addSubmit('send', 'Sign up');

		$form->onSuccess[] = [$this, 'signUpThroughAutenticator'];
		return $form;
	}

    public function signUpFormSucceeded(Form $form, \stdClass $data): void
	{
		try {
			$this->authenticator->newCustomer($data->email, $data->name, $data->surname, $data->password);
			$this->redirect('Sign:in');
            $this->getUser()->login($data->email, $data->password);
			$this->redirect('Homepage:');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Nastala chyba.');
		}
	}

	public function logInWithAuthenticator(Form $form, \stdClass $data): void
	{
		/* přidávání nového uživatele */
		//$this->authenticator->novyUzivatel("filip", "heslo", "personalista");
		
 		$user = $this->getUser();
		$user->setAuthenticator($this->authenticator);
		$user->login($data->email, $data->password);
		
		$this->redirect('Homepage:');
	}

    public function signUpThroughAutenticator(Form $form, \stdClass $data): void
	{
        $this->authenticator->newCustomer($data->email, $data->name, $data->surname, $data->password);
		$this->getUser()->setAuthenticator($this->authenticator);
		
		$this->redirect('Sign:in');
	}
}
