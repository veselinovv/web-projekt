<?php

namespace App\Model;

use Nette;

/**
 * Třída HomepageModel
 * Obsahuje funkce se kterými pracuji v souboru HomepagePresenter.php.
 * 
 * @author Viktor Veselinov
 */
class HomepageModel
{
	use Nette\SmartObject;

	private $db;

	public function __construct(Nette\Database\Connection $db) {
		$this->db = $db;
	}

	/**
    * Funkce productsPreview
    * Vrací pole všech produktů, obsahující id, název, cenu, popis, rating, počet hvězdiček a odkaz na fotku daného produktu.
    *
    * @author Viktor Veselinov
    * @return resultSet
    */
	public function productsPreview() {
        $sql = 'SELECT id,name,price,description, rating, floor(rating) as stars, link
		FROM products ORDER BY rating desc LIMIT 5';
         
        return $this->db->query($sql);
	}
}