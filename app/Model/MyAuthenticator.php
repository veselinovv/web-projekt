<?php

namespace App\Model; 
use Nette;
use Nette\Security;

/**
 * Třída MyAuthenticator
 * Obsahuje funkce se kterými pracuji v souboru AdminPresenter.php.
 * 
 * @author Viktor Veselinov
 */
class MyAuthenticator implements Nette\Security\Authenticator
{
	private $db;
	private $passwords;

	public function __construct(Nette\Database\Explorer $db, Nette\Security\Passwords $passwords) {
		$this->db = $db;
		$this->passwords = $passwords;
	}
	
	/**
    * Funkce newPassword
    * Hashuje heslo a vrací ho.
    *
    * @author Viktor Veselinov
    * @param string $unhashed - heslo v podobě čistého stringu
    * @return string
    */
	public function newPassword($unhashed) : string
	{
		return $this->passwords->hash($unhashed);
	}

	/**
    * Funkce newCustomer
    * Ukládá data nového zákazníka do databáze.
    *
    * @author Viktor Veselinov
    * @param string $email - e-mail zákazníka
	* @param string $name - jméno zákazníka
	* @param string $surname - příjmení zákazníka
	* @param string $password - heslo zákazníka
    */
	public function newCustomer($email, $name, $surname, $password){
		$this->db->table('customers')->insert([
			'email' => $email,
            'name' => $name,
            'surname' => $surname,
			'password' => $this->newPassword($password)
		]);
	}

	/**
    * Funkce authenticate
    * Prověřuje zda uživatel s daným e-mailem uvedl správné heslo tohoto účtu.
    *
    * @author Viktor Veselinov
    * @param string $email - e-mail zákazníka
	* @param string $password - heslo v podobě čistého stringu
    * @return SimpleIdentity
    */
	public function authenticate(string $email, string $password): Security\IIdentity
	{
		$row = $this->db->table('customers')->where('email', $email)->fetch();
		bdump($row);
		if(!$row || !$this->passwords->verify($password, $row->password)) throw new Security\AuthenticationException('Špatné přihlašovací údaje');

		return new Security\SimpleIdentity(
			$row->id,
			$row->role,
            ["email" => $email, "name" => $row->name, "surname" => $row->surname, "role" => $row->role]
		);
	}

}