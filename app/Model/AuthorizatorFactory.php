<?php

namespace App\Model; 
use Nette;

/**
 * Třída AuthorizatorFactory
 * Zde se vytváří role hierarchie a určuje se jaké mají práva.
 * 
 * @author Viktor Veselinov
 */
class AuthorizatorFactory
{	
	/**
    * Funkce create
    * Vytváří a vrací objekt třídy Nette\Security\Permission, ve kterém se ukládají všechny role a jejich práva.
    *
    * @author Viktor Veselinov
    * @return object $acl
    */
	public static function create(): Nette\Security\Permission
	{
		$acl = new Nette\Security\Permission;
	
		$acl->addRole('admin'); //admin
		$acl->addRole('customer');  //zákazník
        $acl->addRole('guest'); //nepřihlášený uživatel

		$acl->addResource('manage_self');   //možnost upravovat svůj profil
		$acl->addResource('manage_customer');   //možnost upravovat profil zákazníka
		$acl->addResource('manage_products');   //možnost upravovat zboží

		//všichni mají možnost sledovat a editovat svůj profil
		$acl->allow($acl::ALL, 'manage_self', ['view', 'edit']);	

		//návštěvník nemá žádná práva
		$acl->deny('guest', $acl::ALL, $acl::ALL);

		//admin má všechna práva
		$acl->allow('admin', $acl::ALL, $acl::ALL);

		return $acl;
	}
};