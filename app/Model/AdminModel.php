<?php

namespace App\Model;

use Nette;

/**
 * Třída AdminModel
 * Obsahuje funkce se kterými pracuji v souboru AdminPresenter.php.
 * 
 * @author Viktor Veselinov
 */
class AdminModel
{
	use Nette\SmartObject;

	private $db;

	public function __construct(Nette\Database\Connection $db) {
		$this->db = $db;
	}

    /**
    * Funkce removeDiacritic
    * Odstraňuje diakritická znaménka z daného řetězce a vrací čistý text. 
    *
    * @author Viktor Veselinov
    * @param string $text - vstupní text
    * @return string
    */
    public function removeDiacritic($text)
    {
        $diac = array('ř','Ř','ú','Ú','ň','Ň','ě','Ě','ľ','š','č','ť','ž','ý','á','í','é','Č','Á','Ž','Ý',' ');
        $cor = array('r','R','u','U','n','N','e','E','l','s','c','t','z','y','a','i','e','C','A','Z','Y','_');
        $text = str_replace($diac,$cor,$text);
        return $text;
    }

    /**
    * Funkce savePhoto
    * Ukládá fotku do souboru, na který odkazujeme a vrací odpověď.
    *
    * @author Viktor Veselinov
    * @param object $photo - objekt fotky
    * @param string $link - odkaz
    * @return boolean
    */
    public function savePhoto($photo, $link){
        if(!$photo->isOk()) return false;
        $this->deletePhoto($link);
        $photo->move($link);
        return true;
    }

    /**
    * Funkce deletePhoto
    * Maže soubor na který odkazujeme a vrací odpověď.
    *
    * @author Viktor Veselinov
    * @param string $link - odkaz
    * @return boolean
    */
    public function deletePhoto($link){
        if(Strings::startsWith($link,  "/")){
            $link = substr($link, 1);
        }
        try{FileSystem::delete($link); return true;}
        catch(Nette\IOException $e){return false;}
    }

    /**
    * Funkce photoExists
    * Kontroluje zda soubor existuje a vrací odpověď.
    *
    * @author Viktor Veselinov
    * @param string $link - odkaz
    * @return boolean
    */
    public function photoExists($link){
        if(Strings::startsWith($link,  "/")){
            $link = substr($link, 1);
        }
        try{FileSystem::read($link); return true;}
        catch(Nette\IOException $e){return false;}
    }

    /**
    * Funkce listProducts
    * Vrací výpis všech produktů v databázi.
    *
    * @author Viktor Veselinov
    * @return resultSet
    */
    public function listProducts()
    {
        return $this->db->query(
            'SELECT id,name,price,description,link FROM products' 
        );
    }

    /**
    * Funkce productsArray
    * Vrací upravené pole všech produktů v databázi.
    *
    * @author Viktor Veselinov
    * @return array
    */
    public function productsArray()
    {
        if(isset($_GET['product'])>0) $result[0] = "Add a product";
        else $result[0] = "Select a product";

        foreach($this->listProducts() as $product){
            $result[$product->id] = $product->name;
        }
        return $result;
    }

    /**
    * Funkce getProduct
    * Vrací data o konkrétném produktu.
    *
    * @author Viktor Veselinov
    * @param int $id - id produktu
    * @return resultSet
    */
    public function getProduct($id)
    {
        return $this->db->fetch(
            'SELECT id,name,price,description,link FROM products 
            WHERE id=?', $id
        );
    }

    /**
    * Funkce newProduct
    * Ukládá data o novém produktu do databáze a vrací zprávu o průběhu funkce.
    *
    * @author Viktor Veselinov
    * @param array $data - data o produktu
    * @param object $photo - objekt fotky
    * @return string
    */
    public function newProduct($data, $photo)
    {
        $link = "img/products/".$this->removeDiacritic($data['name']).".".$photo->getImageFileExtension();
        if(!$this->savePhoto($photo, $link)) return [false, "There was a problem saving the photo."];
        $pRes = $this->db->query('INSERT INTO products', [
            "name" => $data['name'],
            "price" => $data['price'],
            "description" => $data['description'],
            "link" => '/'.$link
        ]);
        if($pRes)
        {
            return "A new product has been added.";
        }
        else return "There was a problem with the SQL query!";
    }

    /**
    * Funkce updateProduct
    * Přepisuje o určitém produktu z databáze a vrací zprávu o průběhu funkce.
    *
    * @author Viktor Veselinov
    * @param int $id - id produktu
    * @param array $data - data o produktu
    * @param object $photo - objekt fotky
    * @return string
    */
    public function updateProduct($id, $data, $photo = null)
    {
        $pRes = $this->db->query('UPDATE products SET', $data, 'WHERE id=?', $id);
        if($pRes)
        {
            return "The data for this product have been changed.";
        }
        else if($photo->getContents() == null) return [true, "The data has been changed, photo remains the same."];
    
        $oldPhoto = $this->db->fetch('SELECT link FROM products WHERE id=?', $id)->link;
        if(!$this->deletePhoto($oldPhoto)) return [false, "There was a problem deleting the photo of the product."];

        $link = "img/products/".$this->removeDiacritic($data['name']).".".$photo->getImageFileExtension();
        if(!$this->savePhoto($photo, $link)) return [false, "There was a problem saving the photo of the product."];
        $this->db->query('UPDATE products SET', [ "link" => '/'.$link ], 'WHERE id=?', $id);

        return [true, "The photo of the product has been changed."];
    }

    /**
    * Funkce updateProduct
    * Maže data o určitém produktu z databáze a vrací zprávu o průběhu funkce.
    *
    * @author Viktor Veselinov
    * @param int $id - id produktu
    * @return string
    */
    public function deleteProduct($id)
    {
        $pRes = $this->db->query('DELETE FROM products WHERE id=?', $id);
        if($pRes)
        {
            return "This product has been deleted.";
        }
        else return "There was a problem with the SQL query!";
    }

    /**
    * Funkce listCustomers
    * Vrací pole s daty všech zákazníků z databáze.
    *
    * @author Viktor Veselinov
    * @return resultSet
    */
    public function listCustomers()
    {
        return $this->db->query(
            'SELECT id,email,name,surname FROM customers
            WHERE role = "customer" ORDER BY id' 
        );
    }

    /**
    * Funkce customersArray
    * Vrací upravené pole s daty všech zákazníků z databáze.
    *
    * @author Viktor Veselinov
    * @return array
    */
    public function customersArray()
    {
        $result[0] = "Select a customer";
        foreach($this->listCustomers() as $customer){
            $result[$customer->id] = $customer->name.' '.$customer->surname.' - '.$customer->email;
        }
        return $result;
    }

    /**
    * Funkce getCustomer
    * Vrací data daného zákazníka podle jeho id.
    *
    * @author Viktor Veselinov
    * @param int $id - id zákazníka
    * @return resultSet
    */
    public function getCustomer($id)
    {
        return $this->db->fetch(
            'SELECT id,email,name,surname,role FROM customers 
            WHERE id=?', $id
        );
    }
	
    /**
    * Funkce updateCustomer
    * Upravuje data určitého zákazníka podle jeho id a vrací zprávu o průběhu funkce.
    *
    * @author Viktor Veselinov
    * @param int $id - id zákazníka
    * @param array $data - data o zákazníkovi
    * @return string
    */
    public function updateCustomer($id, $data)
    {
        $cRes = $this->db->query('UPDATE customers SET', $data, 'WHERE id=?', $id);
        if($cRes)
        {
            return "The data for this customer have been changed.";
        }
        else return "There was a problem with the SQL query!";
    }

    /**
    * Funkce deleteCustomer
    * Maže data určitého zákazníka podle jeho id a vrací zprávu o průběhu funkce.
    *
    * @author Viktor Veselinov
    * @param int $id - id zákazníka
    * @return string
    */
    public function deleteCustomer($id)
    {
        $cRes = $this->db->query('DELETE FROM customers WHERE id=?', $id);
        if($cRes)
        {
            return "The customer's profile has been deleted.";
        }
        else return "There was a problem with the SQL query!";
    }
}